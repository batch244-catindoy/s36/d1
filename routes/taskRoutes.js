// routes - contains all the endpoint for our application

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

// route to all get the tasks
router.get("/", (req, res) =>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// routes to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// route to delete a task
// ":id" is a wildcard
router.delete("/:id", (req, res) => {
				// params - URL parameter
	taskController.deleteTask(req.params.id).then(resultFromController => res.send (resultFromController));
});

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

// Activity
router.get("/:id", (req, res) =>{
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


// export the module
module.exports = router;